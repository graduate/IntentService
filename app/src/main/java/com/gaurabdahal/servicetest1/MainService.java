//Gaurab Dahal (L20432055)

package com.gaurabdahal.servicetest1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

public class MainService extends MyIntentService {
    boolean is_running;
    public  static final String ACTION_START = "com.gaurabdahal.servicetest1.START";
    public  static final String ACTION_STOP = "com.gaurabdahal.servicetest1.STOP";
    public static final String ACTION_MSG1 = "com.gaurabdahal.servicetest1.MESSAGE1";
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public MainService() {
        super("MyService");
        is_running = false;
    }


    // The service is being created
    @Override
    public void onCreate() {
        // do any start initializing here
        super.onCreate();
    }


    // Not using but needed so stub this out (return null)
    @Override
    public IBinder onBind(Intent intent) { return null; }

    /**
     * This method is invoked on the worker thread with a request to process.
     * Only one Intent is processed at a time, but the processing happens on a
     * worker thread that runs independently from other application logic.
     * So, if this code takes a long time, it will hold up other requests to
     * the same IntentService, but it will not hold up anything else.
     * When all requests have been handled, the IntentService stops itself,
     * so you should not call {@link #stopSelf}.
     *
     * @param intent The value passed to {@link
     *               Context#startService(Intent)}.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        if(action.equals(ACTION_START)){
            if (! is_running) {
                Toast.makeText(this,"Service Starting",Toast.LENGTH_LONG).show();
                is_running = true;
            }
        }else if(action.equals(ACTION_STOP)) {
            stopSelf();

        }else if(action.equals(ACTION_MSG1)) {
            Toast.makeText(this,action,Toast.LENGTH_SHORT).show();
            is_running = true;
        }

    }


    // Called when the service is no longer used and is being destroyed
    @Override
    public void onDestroy() {
        Toast.makeText(this,"Service Stopping",Toast.LENGTH_SHORT).show();
        super.onDestroy();
        is_running = false;
    }
}
