//Gaurab Dahal (L20432055)

package com.gaurabdahal.servicetest1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button buttonStart, buttonStop, buttonMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonStart   = (Button) findViewById(R.id.buttonStart);
        buttonStop    = (Button) findViewById(R.id.buttonStop);
        buttonMessage = (Button) findViewById(R.id.buttonMessage);
        buttonStart.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
        buttonMessage.setOnClickListener(this);
    }
    public void onClick(View v) {
        if (v == buttonStart) {
            Intent intent = new Intent(this, MainService.class);
            intent.setAction(MainService.ACTION_START);
            startService(intent)  ;
        }
        else if (v == buttonStop) {
            Intent intent = new Intent(this, MainService.class);
            intent.setAction(MainService.ACTION_STOP);
            startService(intent)  ;
        }
        else if (v == buttonMessage) {
            Intent intent = new Intent(this, MainService.class);
            intent.setAction(MainService.ACTION_MSG1);
            startService(intent)  ;
        }
    }
}